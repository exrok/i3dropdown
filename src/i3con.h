#ifndef __I3CON_H_
#define __I3CON_H_


typedef unsigned char uchar;
#define _QREF(a,b)  sizeof( a ) -1 ,  b

#define _INT_PAD "1234567890" 
#define _LONG_PAD "1234567890123456789012345678901234567890123456"
#define _I3_PACKET "i3-ipcAAAABBBB"
#define _base_hide _I3_PACKET _LONG_PAD " move scratchpad"
#define _base_show _I3_PACKET _LONG_PAD " move workspace current"
#define _base_pos  _I3_PACKET _LONG_PAD " resize set " _INT_PAD " " _INT_PAD ";" \
                              _LONG_PAD " move position " _INT_PAD " " _INT_PAD
const int _ref_pos[] ={_QREF(_I3_PACKET _LONG_PAD " resize set ", _QREF(_INT_PAD " ", _QREF( _INT_PAD ";"
                   , _QREF(_LONG_PAD " move position ", _QREF(_INT_PAD " ", _QREF( _INT_PAD, 0))))))};

struct I3Window {
    int sockfd;
    I3Window(int sockfd);

    void target_id(unsigned long int id);
    void target_instance(const char *inst);
    void hide();
    void show();
    void resize(int width, int height);
    void position(int x, int y);
private:
    int init_state;
    char _hide[sizeof _base_hide];
    char _show[sizeof _base_show];
    char _pos[sizeof _base_pos];
    char *pos_ref[5];
};


#endif // __I3CON_H_
