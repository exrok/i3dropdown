#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include "i3ipc.h"
#include "i3con.h"
#include <stdio.h>


char *puts_buf(char *buf,const char *str, int max = -1){
    while(*str != '\0' && (max--) !=0){
        *(buf++) = *(str++);
    }
    return buf;
}

void init_i3ipc_header(char *buf, const uint32_t message_size,
                       const uint32_t message_type){
    const i3_ipc_header_t header = {
        /* We don’t use I3_IPC_MAGIC because it’s a 0-terminated C string. */
        .magic = {'i', '3', '-', 'i', 'p', 'c'},
        .size = message_size,
        .type = I3_IPC_MESSAGE_TYPE_RUN_COMMAND};

   memcpy(buf, (char*)((void *)&header), sizeof(i3_ipc_header_t));
}
void buf_fill(char *a, char *b, char fill_char = ' '){
    for (; a < b; ) *(a++) = ' '; 
}

char * buf_ulong(char *p, unsigned long int n){
    unsigned long N = n, rev, count = 0;
    rev = N;
    if (N == 0) {*(p++)='0'; return p;}
    while ((rev % 10) == 0) {count++; rev /= 10;}
    rev = 0;
    while (N != 0) {
        rev = (rev << 3) + (rev << 1) + N % 10; N /=10;
    }
    while (rev != 0){
        *(p++)=rev % 10 + '0';
        rev /= 10;
    }
    while (count--) *(p++) = '0';
    return p;
}

char * buf_int(char *p,int n){
   if (n < 0){
       *(p++) = '-';
       n *= -1;
   } 
   return buf_ulong(p,n);
}

void insert_con_inst( char *buf,const char *instance){
    char *p = buf;
    p = puts_buf(p, "[instance=\"^");
    p = puts_buf(p, instance, 32);
    p = puts_buf(p, "\"]");
    while(p != buf + sizeof(_LONG_PAD) -1){
        *(p++) = ' '; 
    }
}
void insert_con_id(char *buf, unsigned long int id){
    char *p = buf;
    p = puts_buf(p, "[con_id=");
    p = buf_ulong(p, id);
    p = puts_buf(p, "]");
    while(p != buf + sizeof(_LONG_PAD) -1){
        *(p++) = ' '; 
    }
}

I3Window::I3Window(int sockfd): sockfd{sockfd}, init_state{0},
    _hide{_base_hide},_show{_base_show},_pos{_base_pos}{
    int t = 0;
    for (int i = 0; i< 5; i++){
        t += _ref_pos[i];
        pos_ref[i] = _pos + t;
    }
    init_i3ipc_header(_hide, sizeof(_hide) - sizeof(i3_ipc_header_t),
                      I3_IPC_MESSAGE_TYPE_RUN_COMMAND);
    init_i3ipc_header(_show, sizeof(_show)  - sizeof(i3_ipc_header_t),
                      I3_IPC_MESSAGE_TYPE_RUN_COMMAND);
    init_i3ipc_header(_pos, sizeof(_pos) - sizeof(i3_ipc_header_t),
                      I3_IPC_MESSAGE_TYPE_RUN_COMMAND);
   }                                                             

void I3Window::target_id(unsigned long int id){
    insert_con_id(_hide + sizeof(_I3_PACKET) -1, id);
    insert_con_id(_show + sizeof(_I3_PACKET) -1 , id);
    insert_con_id(_pos + sizeof(_I3_PACKET) -1, id);
    insert_con_id(pos_ref[2], id);
}
void I3Window::target_instance(const char *inst){
    insert_con_inst(_hide + sizeof(_I3_PACKET) -1, inst);
    insert_con_inst(_show + sizeof(_I3_PACKET) -1, inst);
    insert_con_inst(_pos + sizeof(_I3_PACKET) -1, inst);
    insert_con_inst(pos_ref[2], inst);
}
void I3Window::hide(){
    i3ipc_xrun(sockfd, sizeof(_hide), _hide);
}
void I3Window::show(){
    i3ipc_xrun(sockfd, sizeof(_show), _show);
}
void I3Window::resize(int width, int height){
    init_state = init_state | 1;
    buf_fill(buf_int(pos_ref[0], width), pos_ref[0] + sizeof(_INT_PAD) -1);
    buf_fill(buf_int(pos_ref[1], height), pos_ref[1] + sizeof(_INT_PAD) - 1);
    if (init_state==3) i3ipc_xrun(sockfd, sizeof(_pos), _pos);
}
void I3Window::position(int x, int y){
    init_state = init_state | 2;
    buf_fill(buf_int(pos_ref[3], x), pos_ref[3] + sizeof(_INT_PAD));
    buf_fill(buf_int(pos_ref[4], y), pos_ref[4] + sizeof(_INT_PAD));
    if (init_state==3) i3ipc_xrun(sockfd, sizeof(_pos), _pos);
}


